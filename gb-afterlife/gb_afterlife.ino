/**************************
 *
 * Alan Schultz
 * This sketch provides a hybrid Afterlife, video game and party mode
 * proton pack.
 *************************/

#include <Audio.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <ObjectFLED.h>
#include <FastLED.h>
#include <Toggle.h>
#include <AsyncTimer.h>

// LEDS
const uint8_t cyclotronPin[1] = {40};
#define NUM_LEDS_CYCLOTRON 204 
const uint8_t corePin[1] = {39};
#define NUM_LEDS_CORE 72
const uint8_t powercellPin[1] = {41};
#define NUM_LEDS_POWERCELL 16

CRGB powercell_leds[NUM_LEDS_POWERCELL];
CRGB cyclotron_leds[NUM_LEDS_CYCLOTRON];
CRGB core_leds[NUM_LEDS_CORE];

const int slobloLEDPin = 33;

// GRB
uint32_t curColor = 0x00FF00;

ObjectFLED powercellStrip(NUM_LEDS_POWERCELL, powercell_leds, CORDER_RGB, 1, powercellPin);
ObjectFLED cyclotronStrip(NUM_LEDS_CYCLOTRON, cyclotron_leds, CORDER_RGB, 1, cyclotronPin);
ObjectFLED coreStrip(NUM_LEDS_CORE, core_leds, CORDER_RGB, 1, corePin);

// buttons/toggles
const byte intensifyPin = A10;
const byte secondaryPin = A11;
const byte slobloPin = A12;
const byte ventPin = A13;
const byte activatePin = A14;
Toggle intensifyButton(intensifyPin); 
Toggle secondaryButton(secondaryPin); 
Toggle slobloToggle(slobloPin); 
Toggle ventToggle(ventPin);
Toggle activateToggle(activatePin);

// timers
unsigned short overheatTimer = -1;
unsigned short coreTimer = -1;
unsigned short cyclotronTimer = -1;
unsigned short powercellTimer = -1; 
unsigned short firingTimer = -1; 
unsigned short idleTimer = -1; 
unsigned short ventTimer = -1; 
AsyncTimer t;

// global variables
boolean isVenting = false;
boolean isFiring = false;
boolean isSloBlo = false;
boolean isCrossingStreams = false;
boolean partyMode = false;
int randomFire = -1;
int overheatSeconds = 0;
int curSong = 1;
String curfile = "";


// Audio variables
AudioPlaySdWav           playIdle;     //xy=148,133
AudioPlaySdWav           playFiring;     //xy=151,198
AudioPlaySdWav           playStartup;     //xy=159,79
AudioMixer4              mixer2;         //xy=366,194
AudioMixer4              mixer1;         //xy=369,87
AudioAmplifier           amp1;           //xy=512,107
AudioAmplifier           amp2;           //xy=516,190
AudioOutputI2S           i2s1;           //xy=648,157
AudioConnection          patchCord1(playIdle, 0, mixer1, 2);
AudioConnection          patchCord2(playIdle, 1, mixer1, 3);
AudioConnection          patchCord3(playFiring, 0, mixer2, 0);
AudioConnection          patchCord4(playFiring, 1, mixer2, 1);
AudioConnection          patchCord5(playStartup, 0, mixer1, 0);
AudioConnection          patchCord6(playStartup, 1, mixer1, 1);
AudioConnection          patchCord7(mixer2, amp2);
AudioConnection          patchCord8(mixer1, amp1);
AudioConnection          patchCord9(amp1, 0, i2s1, 0);
AudioConnection          patchCord10(amp2, 0, i2s1, 1);
AudioControlSGTL5000 sgtl5000_1;

// Use these with the Teensy Audio Shield
#define SDCARD_CS_PIN BUILTIN_SDCARD
#define SDCARD_MOSI_PIN 11  // Teensy 4 ignores this, uses pin 11
#define SDCARD_SCK_PIN 13   // Teensy 4 ignores this, uses pin 13

void setup() {
  Serial.begin(9600);

  // Audio connections require memory to work.  For more
  // detailed information, see the MemoryAndCpuUsage example
  AudioMemory(12);

  sgtl5000_1.enable();
  sgtl5000_1.lineOutLevel(31);
  sgtl5000_1.volume(.5);
  sgtl5000_1.enhanceBassEnable();
  mixer1.gain(1, 1.5);
  mixer2.gain(1, 1.5);
  amp1.gain(1.5);
  amp2.gain(1.5);

  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);

  Serial.println("accessing SD card");
  if (!(SD.begin(SDCARD_CS_PIN))) {
    // stop here, but print a message repetitively
    while (1) {
      Serial.println("Unable to access the SD card");
      delay(500);
    }
  }

  // LEDs
  powercellStrip.setBrightness(50);
  powercellStrip.begin();
  cyclotronStrip.setBrightness(50);
  cyclotronStrip.begin();
  coreStrip.setBrightness(50);
  coreStrip.begin();

  powercellTimer = t.setInterval(onPowercellExpire, 80);
  cyclotronTimer = t.setInterval(onCyclotronExpire, 30);
  coreTimer = t.setInterval(onCoreExpire, 30);

  pinMode(slobloLEDPin, OUTPUT);
  int slobloState = analogRead(slobloPin) > 100 ? LOW : HIGH;
  digitalWrite(slobloLEDPin, slobloState);

  intensifyButton.begin(intensifyPin);
  secondaryButton.begin(secondaryPin);
  slobloToggle.begin(slobloPin);
  ventToggle.begin(ventPin);
  activateToggle.begin(activatePin);

  if (analogRead(intensifyPin) < 100) {
      Serial.println("party mode");
      partyMode = true;
    } else {
      playStartup.play("startup_afterlife.wav");
      delay(500);

      playIdle.play("idle0_b.wav");
      
      delay(25);
      idleTimer = t.setTimeout(onIdleExpire, playIdle.lengthMillis());
    }

}

void loop() {
  t.handle();
  intensifyButton.poll();
  secondaryButton.poll();
  slobloToggle.poll();

  if (!partyMode) {
    if (intensifyButton.onPress()) onIntensifyButtonPressed();
    if (intensifyButton.onRelease()) onIntensifyButtonReleased();
    if (secondaryButton.onPress()) onSecondaryButtonPressed();
    if (secondaryButton.onRelease()) onSecondaryButtonReleased();
    if (slobloToggle.DNtoMID()) onSlobloOn();
    if (slobloToggle.MIDtoDN()) onSlobloOff();
  } else {
    if (intensifyButton.onPress()) playSong();
  }
    
  
}

void onIdleExpire() {
  Serial.println("replay Idle");
  playIdle.play("idle0_b.wav");
  delay(25);
  idleTimer = t.setTimeout(onIdleExpire, playIdle.lengthMillis());
}

void onIntensifyButtonPressed() {
  Serial.println("Intensify Button pressed");
  Serial.println(digitalRead(intensifyPin));
  t.cancel(firingTimer);
  playFiringWav("fire1_h.wav");
  firingTimer = t.setTimeout(onStartFiringEnd, playFiring.lengthMillis());
  if (!isVenting) {
    if (isSloBlo) {
      playFiringWav("fire1_h.wav");
    } else {
      overheatTimer = t.setInterval(updateOverheatSeconds, 1000);
      // random pick from 2 blaster tracks
      randomFire = rand() % 2;
      if (randomFire == 1) {
        playFiringWav("fire0_h.wav");
      } else {
        playFiringWav("alt0_h1.wav");
      }
    }
  } else {
    playFiringWav("dryvent.wav");
  }

  t.cancel(cyclotronTimer);
  t.cancel(coreTimer);
  cyclotronTimer = t.setInterval(onCyclotronExpire, 1);
  coreTimer = t.setInterval(onCoreExpire, 5);
  firingTimer = t.setTimeout(onStartFiringEnd, playFiring.lengthMillis());
}

void onStartFiringEnd() {
  if (!isVenting && intensifyButton.isPressed()) {
    if (isSloBlo && !isPlaying("fire1_h.wav") && !isPlaying("fire1_b.wav")) {
      playFiringWav("fire1_b.wav");
    } else if (!isSloBlo && !isPlaying("alt0_h1.wav") && !isPlaying("fire0_h.wav")) {
      if (overheatSeconds > 8 && !isPlaying("fire0_w.wav") && !isPlaying("alt0_w.wav")) {
        Serial.println("begin overheat warning");
        if (randomFire == 1) {
          playFiringWav("fire0_w.wav");
        } else {
          playFiringWav("alt0_w.wav");
        }
      } else if (!isPlaying("alt0_b.wav") 
        && !isPlaying("fire0_w.wav") 
        && !isPlaying("fire0_b.wav")
        && !isPlaying("alt0_w.wav")) {
        if (randomFire == 1) {
          playFiringWav("fire0_b.wav");
        } else {
          playFiringWav("alt0_b.wav");
        }
        
      }
    }
    firingTimer = t.setTimeout(onStartFiringEnd, playFiring.lengthMillis());
  }
}

void onIntensifyButtonReleased() {
  Serial.println("Intensify Button released");
  Serial.println(digitalRead(intensifyPin));
  t.cancel(firingTimer);
  if (!isVenting && !isCrossingStreams) {
    if (isSloBlo) {
      playFiringWav("fire1_t.wav");
    } else {
      if (overheatSeconds > 7) {
        playFiringWav("alt0_t3.wav");
      } else if (overheatSeconds > 4) {
        playFiringWav("alt0_t2.wav");
      } else {
        playFiringWav("alt0_t1.wav");
      }
      Serial.println("stop firing");
      t.cancel(overheatTimer);
    }
  }

  t.cancel(cyclotronTimer);
  t.cancel(coreTimer);

  cyclotronTimer = t.setInterval(onCyclotronExpire, 30);
  coreTimer = t.setInterval(onCoreExpire, 30);
}

void onSecondaryButtonPressed() {
  Serial.println("Seondary Button pressed");
  randomFire = rand() % 2;
  if (randomFire == 1) {
    playFiringWav("alt1_h.wav");
  } else {
    playFiringWav("alt2_h.wav");
  }
}

void onSecondaryButtonReleased() {
  Serial.println("Seondary Button released");
}

void playFiringWav(String name) {
  //set current file for is playing reference
  curfile = name;
  playFiring.play(curfile.c_str());
  delay(25);
}

boolean isPlaying(String name) {
  if (name == curfile && playFiring.isPlaying()) {
    return true;
  } else {
    return false;
  }
}

void onSlobloOn() {
  Serial.print("SloBlo toggle has been activated");
  digitalWrite(slobloLEDPin, LOW);
}
void onSlobloOff() {
  Serial.print("SloBlo toggle has been deactivated");
  digitalWrite(slobloLEDPin, HIGH);
}

void ventActivated(uint8_t value) {
  Serial.print("Vent toggle ");
  Serial.print(value);
  Serial.println(" has been activated");
}
void ventDeactivated(uint8_t value) {
  Serial.print("Vent toggle ");
  Serial.print(value);
  Serial.println(" has been deactivated");
}

void playSong() {
  Serial.print("Play song ");

  String filename;
  filename = "song";
  filename += curSong;
  filename += ".wav";
  Serial.println(filename);

  t.cancel(idleTimer);

  playIdle.play(filename.c_str());
  delay(25);
  curSong++;
  if (curSong == 16) {
    curSong = 1;
  }
  idleTimer = t.setTimeout(playSong, playIdle.lengthMillis());
}

void updateOverheatSeconds() {
  Serial.print("Overheat timer seconds=");
  overheatSeconds += 1;
  Serial.println(overheatSeconds);
  if (overheatSeconds == 11) {
    t.cancel(overheatTimer);
    ventOverride();
  }
}

void ventOverride() {
  isVenting = true;
  t.cancel(firingTimer);
  playFiring.stop();
  playStartup.play("vent2.wav");
  delay(25);
  ventTimer = t.setTimeout(onVentExpire, playStartup.lengthMillis());
  
}

void onVentExpire() {
  overheatSeconds = 0;
  isVenting = false;
  playStartup.play("pwrup.wav");
}

void onPowercellExpire() {
  static int curPowercell = 0;
  if (curPowercell == NUM_LEDS_POWERCELL) {
    fill_solid(powercell_leds, NUM_LEDS_POWERCELL, CRGB::Black);
    curPowercell = 0;
  }

  powercell_leds[curPowercell] = CRGB::Blue; 
  curPowercell++;
  powercellStrip.show();
}

void onCoreExpire() {
  static int currentCore = 0;
  if (currentCore == NUM_LEDS_CORE) {
    fill_solid(core_leds, NUM_LEDS_CORE, CRGB::Black);
    currentCore = 0;
  }

  if (currentCore > 9) {
    // core_leds[currentCore - 10] = CRGB::Black; 
    fadeToBlackBy(core_leds, currentCore - 10, 100);
  }

  core_leds[currentCore] = curColor;
  currentCore++;
  coreStrip.show();
}

void onCyclotronExpire() {  
  static int currentCyclotron = 0;
  if (currentCyclotron == NUM_LEDS_CYCLOTRON) {
    fill_solid(cyclotron_leds, NUM_LEDS_CYCLOTRON, CRGB::Black);
    currentCyclotron = 0;
  }

  if (currentCyclotron > 19) {
    // cyclotron_leds[currentCyclotron - 20] = CRGB::Black; 
    fadeToBlackBy(cyclotron_leds, currentCyclotron - 20, 100);
  }

  cyclotron_leds[currentCyclotron] = curColor;
  currentCyclotron++;
  cyclotronStrip.show();
}