/**************************
 *
 * Alan Schultz
 * This sketch provides a slave attached arduino mega to listen for neutrona wand button events
 * (SLAVE)
 *************************/
#include <Wire.h>
#include "FastLED.h"
#include "Timer.h"                     //http://github.com/JChristensen/Timer
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>

/**
 * Neopixels
 */
#define NEO_CYCLOTRON_PIN 22
#define NUM_LEDS_CYCLOTRON 35
CRGB cyclotron_leds[NUM_LEDS_CYCLOTRON];
int curCyclotron = 0;
int cyclotronTimer = -1;

#define CYCLOTRON_SLOW 1200
#define CYCLOTRON_FAST 300

// These are the indexes for the led's on the chain. Each jewel has 7 LEDs. 
int c1Start = 7;
int c1End = 13;
int c2Start = 0;
int c2End = 6;
int c3Start = 21;
int c3End = 27;
int c4Start = 14;
int c4End = 20;
int ventStart = 28;
int ventEnd = 34;

#define NEO_POWERCELL_PIN 25
#define NUM_LEDS_POWERCELL 16
CRGB powercell_leds[NUM_LEDS_POWERCELL];
int curPowercell = 0;
int powercellTimer = -1;

#define NEO_SIDE_BAR_1_PIN 26
#define NUM_LEDS_SIDEBAR 14
CRGB sidebar1_leds[NUM_LEDS_SIDEBAR];

#define NEO_SIDE_BAR_2_PIN 27
CRGB sidebar2_leds[NUM_LEDS_SIDEBAR];
int sidebarTimer = -1;

/**
 * Hats/leds
 */
int firingHat = A4; //firing/not firing (orang hat barrel of thrower)
int ventHat = A5; //blink while firing (white top of thrower)
int slobloHat = A7;
int ventLight = A8;

boolean isFiring = 0;
boolean firingStarted = 0;
boolean firingStopped = 0;
boolean isSloBlo = 0;
boolean isVenting = 0;
boolean didVent = 0;
int overheatCount = 0;

Timer t;

//// def gfx
#define cs 8
#define rst 9
#define dc 10
#define mosi 11
#define sclk 12
int gfxTimer = -1;

Adafruit_ST7735 tft = Adafruit_ST7735(cs, dc, mosi, sclk, rst);


void setup() {
  Serial.begin(9600);

  // initialize a ST7735S chip, black tab
//  tft.initR(INITR_BLACKTAB);
//  tft.fillScreen(ST7735_BLACK);
  
   //tft.setRotation(tft.getRotation()+1); //uncomment to rotate display
//  tft.fillScreen(ST7735_BLACK);
  
  // Start the I2C Bus as Slave on address 1
  Wire.begin(1);
  // Attach a function to trigger when something is received.
  Wire.onReceive(receiveEvent);

  //Setup leds
  pinMode(firingHat, OUTPUT);
  pinMode(ventHat, OUTPUT);
  pinMode(slobloHat, OUTPUT);
  pinMode(ventLight, OUTPUT);

//  set_max_power_in_volts_and_milliamps(5, 500);
  
  FastLED.addLeds<NEOPIXEL, NEO_CYCLOTRON_PIN>(cyclotron_leds, NUM_LEDS_CYCLOTRON);
  FastLED.addLeds<NEOPIXEL, NEO_POWERCELL_PIN>(powercell_leds, NUM_LEDS_POWERCELL);
  FastLED.addLeds<NEOPIXEL, NEO_SIDE_BAR_1_PIN>(sidebar1_leds, NUM_LEDS_SIDEBAR);
  FastLED.addLeds<NEOPIXEL, NEO_SIDE_BAR_2_PIN>(sidebar2_leds, NUM_LEDS_SIDEBAR);
  
  FastLED.setBrightness(50);
  FastLED.clear(true);

  for (int i = c1Start; i < c1End; i++) {
    cyclotron_leds[i] = CRGB::Red;
  }
  curCyclotron++;
  
//  show_at_max_brightness_for_power();
  
//  FastLED.show();

  cyclotronTimer = t.every(CYCLOTRON_SLOW, updateCyclotron);
  powercellTimer = t.every(50, updatePowercell);
//  gfxTimer = t.every(50, updateGfx);

//  tftPrintTest();
//  delay(500);
//  tft.fillScreen(ST7735_BLACK);
//  testfastlines(ST7735_RED, ST7735_BLUE);
//  delay(500);
//  testdrawrects(ST7735_GREEN);
//  delay(500);
//  tft.fillScreen(ST7735_BLACK);
}

void loop() {
  t.update();
  
  if(isFiring) {
    if(!firingStarted) {
      firingStopped = 0;
      firingStarted = 1;
      t.stop(cyclotronTimer);
      cyclotronTimer = t.every(CYCLOTRON_FAST, updateCyclotron);
      sidebarTimer = t.every(11000/16, updateOverheat);
    }
  } else if (!isFiring) {
    if(!firingStopped) {
      firingStopped = 1;
      firingStarted = 0;
      t.stop(cyclotronTimer);
      t.stop(sidebarTimer);
      t.every(CYCLOTRON_SLOW, updateCyclotron);
    }
  }
  if(isVenting) {
    if(!didVent) {
      didVent = !didVent;
      clearOverheat();
    }
  }
  FastLED.show();
//  set_max_power_in_volts_and_milliamps();
}

#define EVENT_FIRING_START 1
#define EVENT_FIRING_END 2
#define EVENT_VENT_START 3
#define EVENT_VENT_END 4
#define EVENT_SLOBLO_ON 5
#define EVENT_SLOBLO_OFF 6
void receiveEvent(int numBytes) {
  switch(Wire.read()) {
    case EVENT_FIRING_START:
      isFiring = 1;
      break;
    case EVENT_FIRING_END:
      isFiring = 0;
      break;
    case EVENT_VENT_START:
      isVenting = 1;
      break;
    case EVENT_VENT_END:
      isVenting = 0;
      break;
    case EVENT_SLOBLO_ON:
      isSloBlo = 1;
      digitalWrite(slobloHat, HIGH);
      break;
    case EVENT_SLOBLO_OFF:
      isSloBlo = 0;
      digitalWrite(slobloHat, LOW);
      break;
  }
}

void updateCyclotron() {
  switch(curCyclotron) {
    case 0:
      for (int i = c4Start; i < c4End; i++) {
        cyclotron_leds[i] = CRGB::Black;
      }
      for (int i = c1Start; i < c1End; i++) {
        cyclotron_leds[i] = CRGB::Red;
      }
      curCyclotron++;
      break;

    case 1:
      for (int i = c1Start; i < c1End; i++) {
        cyclotron_leds[i] = CRGB::Black;
      }
      for (int i = c2Start; i < c2End; i++) {
        cyclotron_leds[i] = CRGB::Red;
      }
      curCyclotron++;
      break;

    case 2:
      for (int i = c2Start; i < c2End; i++) {
        cyclotron_leds[i] = CRGB::Black;
      }
      for (int i = c3Start; i < c3End; i++) {
        cyclotron_leds[i] = CRGB::Red;
      }
      curCyclotron++;
      break;
      
    case 3: 
      for (int i = c3Start; i < c3End; i++) {
        cyclotron_leds[i] = CRGB::Black;
      }
      for (int i = c4Start; i < c4End; i++) {
        cyclotron_leds[i] = CRGB::Red;
      }
      curCyclotron = 0;
     break;
  }
}

void updateOverheat() {
  sidebar1_leds[overheatCount] = CRGB::Yellow;
  sidebar2_leds[overheatCount] = CRGB::Yellow;
  overheatCount++;
}

void clearOverheat() {
  for(int i = 0; i < 14; i++) {
    sidebar1_leds[i] = CRGB::Black;
    sidebar2_leds[i] = CRGB::Black;
  }
  overheatCount = 0;
}

void updatePowercell() {
  if (curPowercell == NUM_LEDS_POWERCELL) {
    for (int i = 0; i < NUM_LEDS_POWERCELL; i++) {
      powercell_leds[i] = CRGB::Black;
    }
    curPowercell = 0;
  }
  powercell_leds[NUM_LEDS_POWERCELL - curPowercell - 1] = CRGB::Blue;
  curPowercell++;
}

void updateGfx() {
  int w = 15;
  int rx = 15;
  int ry = 15;
  
  for (int n = 0; n < 1; n++) {
    fillArc(110, 20, 0, 90, rx-n*w, ry-n*w, w, 0xF800);
    fillArc(110, 60, 0, 90, rx-n*w, ry-n*w, w, 0xF800);
    fillArc(110, 100, 0, 90, rx-n*w, ry-n*w, w, 0xF800);
    fillArc(110, 140, 0, 90, rx-n*w, ry-n*w, w, 0xF800);
  }
  for (int n = 0; n < 1; n++) {
    fillArc(110, 20, 0, 90, rx-n*w, ry-n*w, w, 0x000);
    fillArc(110, 60, 0, 90, rx-n*w, ry-n*w, w, 0x000);
    fillArc(110, 100, 0, 90, rx-n*w, ry-n*w, w, 0x000);
    fillArc(110, 140, 0, 90, rx-n*w, ry-n*w, w, 0x000);
  }
//  tft.fillScreen(ST7735_BLACK);
}

// #########################################################################
// Draw a circular or elliptical arc with a defined thickness
// #########################################################################

// x,y == coords of centre of arc
// start_angle = 0 - 359
// seg_count = number of 3 degree segments to draw (120 => 360 degree arc)
// rx = x axis radius
// yx = y axis radius
// w  = width (thickness) of arc in pixels
// colour = 16 bit colour value
// Note if rx and ry are the same then an arc of a circle is drawn
#define DEG2RAD 0.0174532925
int fillArc(int x, int y, int start_angle, int seg_count, int rx, int ry, int w, unsigned int colour)
{

  byte seg = 3; // Segments are 3 degrees wide = 120 segments for 360 degrees
  byte inc = 3; // Draw segments every 3 degrees, increase to 6 for segmented ring

    // Calculate first pair of coordinates for segment start
    float sx = cos((start_angle - 90) * DEG2RAD);
    float sy = sin((start_angle - 90) * DEG2RAD);
    uint16_t x0 = sx * (rx - w) + x;
    uint16_t y0 = sy * (ry - w) + y;
    uint16_t x1 = sx * rx + x;
    uint16_t y1 = sy * ry + y;

  // Draw colour blocks every inc degrees
  for (int i = start_angle; i < start_angle + seg * seg_count; i += inc) {

    // Calculate pair of coordinates for segment end
    float sx2 = cos((i + seg - 90) * DEG2RAD);
    float sy2 = sin((i + seg - 90) * DEG2RAD);
    int x2 = sx2 * (rx - w) + x;
    int y2 = sy2 * (ry - w) + y;
    int x3 = sx2 * rx + x;
    int y3 = sy2 * ry + y;

    tft.fillTriangle(x0, y0, x1, y1, x2, y2, colour);
    tft.fillTriangle(x1, y1, x2, y2, x3, y3, colour);

    // Copy segment end to sgement start for next segment
    x0 = x2;
    y0 = y2;
    x1 = x3;
    y1 = y3;
  }
}


void testfastlines(uint16_t color1, uint16_t color2) {
  tft.fillScreen(ST7735_BLACK);
  for (int16_t y=0; y < tft.height(); y+=5) {
    tft.drawFastHLine(0, y, tft.width(), color1);
  }
  for (int16_t x=0; x < tft.width(); x+=5) {
    tft.drawFastVLine(x, 0, tft.height(), color2);
  }
}

void testdrawrects(uint16_t color) {
  tft.fillScreen(ST7735_BLACK);
  for (int16_t x=0; x < tft.width(); x+=6) {
    tft.drawRect(tft.width()/2 -x/2, tft.height()/2 -x/2 , x, x, color);
  }
}

void tftPrintTest() {
  tft.setTextWrap(false);
  tft.fillScreen(ST7735_BLACK);
  tft.setCursor(0, 10);
  tft.setTextColor(ST7735_WHITE);
  tft.setTextSize(1);
  tft.println("INSTRUCTABLES.COM");
  delay(500);
  tft.setCursor(0, 60);
  tft.setTextColor(ST7735_RED);
  tft.setTextSize(2);
  tft.println("temperature");
  tft.setTextColor(ST7735_YELLOW);
  tft.setTextSize(2);
  tft.println("humidity");
  tft.setTextColor(ST7735_GREEN);
  tft.setTextSize(2);
  tft.println("monitor");
  tft.setTextColor(ST7735_BLUE);
   delay(50);
  }
