/**************************
 *
 * Alan Schultz
 * This sketch provides a hybrid screen, video game and party mode
 * proton pack.
 * (MASTER)
 *
 * Note for Mega 2560: See https://paulhartigan.net/2012/01/01/wave-shield-and-arduino-mega-2650/
 * for correct pin configuration. SD card reader on Wave Shield needs the following:
 * 10 –>53
 * 11 –>51
 * 12 –>50
 * 13 –>52
 *
 *************************/
#include <FatReader.h>
#include <SdReader.h>
#include <avr/pgmspace.h>
#include <AccelStepper.h>
#include <Wire.h>
#include "WaveUtil.h"
#include "WaveHC.h"
#include "Timer.h"                     //http://github.com/JChristensen/Timer

SdReader card;    // This object holds the information for the card
FatVolume vol;    // This holds the information for the partition on the card
FatReader root;   // This holds the information for the filesystem on the card
FatReader f;      // This holds the information for the file we're play

WaveHC wave;      // This is the only wave (audio) object, since we will only play one at a time

#define DEBOUNCE 5  // button debouncer

// here is where we define the buttons that we'll use. button "1" is the first, button "6" is the 6th, etc 
int intensify = A0; // Analog 0 - Intensify
int sloblo = A1; // slo-blo switch
int secondary = A2; // 7 - Secondary Fire
int vent = A3; // vent switch

int activate = 30; // 30 - activate pack

byte buttons[] = {intensify, sloblo, secondary, vent};

boolean isIdle = true;
boolean isVenting = false;
boolean isStarted = false;
boolean isSloBlo = false;
boolean isCrossingStreams = false;
int randomFire = -1;

// timers
int overheatTimer = -1;
int overheatSecondsTimer = -1;
int overheatSeconds = 0;

/**
 * Its Miller Time!
 */
boolean partyMode = false;
int curSong = 1;

Timer t;                               //instantiate the timer object

String curfile = "";

// This handy macro lets us determine how big the array up above is, by checking the size
#define NUMBUTTONS sizeof(buttons)
#define NUMCYCLOTRON sizeof(cyclotron)
// we will track if a button is just pressed, just released, or 'pressed' (the current state
volatile byte pressed[NUMBUTTONS], justpressed[NUMBUTTONS], justreleased[NUMBUTTONS];

// this handy function will return the number of bytes currently free in RAM, great for debugging!
int freeRam(void)
{
  extern int  __bss_end;
  extern int  *__brkval;
  int free_memory;
  if ((int)__brkval == 0) {
    free_memory = ((int)&free_memory) - ((int)&__bss_end);
  }
  else {
    free_memory = ((int)&free_memory) - ((int)__brkval);
  }
  return free_memory;
}

void sdErrorCheck(void)
{
  if (!card.errorCode()) return;
  putstring("\n\rSD I/O error: ");
  Serial.print(card.errorCode(), HEX);
  putstring(", ");
  Serial.println(card.errorData(), HEX);
  while (1);
}

const int stepsPerRevolution = 700;  // change this to fit the number of steps per revolution
// for your motor

AccelStepper stepper(AccelStepper::FULL4WIRE, 6, 7, 8, 9);

int stepCount = 0;  // number of steps the motor has taken

void setup() {
  byte i;
  
  // set up serial port
  Serial.begin(9600);
  putstring_nl("WaveHC with ");
  Serial.print(NUMBUTTONS, DEC);
  putstring_nl("buttons");

  putstring("Free RAM: ");       // This can help with debugging, running out of RAM is bad
  Serial.println(freeRam());      // if this is under 150 bytes it may spell trouble!

  // Set the output pins for the DAC control. This pins are defined in the library
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);

  // pin13 LED
  pinMode(13, OUTPUT);

  // Make input & enable pull-up resistors on switch pins
  for (i = 0; i < NUMBUTTONS; i++) {
    pinMode(buttons[i], INPUT_PULLUP);
  }
  
  //  if (!card.init(true)) { //play with 4 MHz spi if 8MHz isn't working for you
  if (!card.init()) {         //play with 8 MHz spi (default faster!)
    putstring_nl("Card init. failed!");  // Something went wrong, lets print out why
    sdErrorCheck();
    while (1);                           // then 'halt' - do nothing!
  }

  // enable optimize read - some cards may timeout. Disable if you're having problems
  card.partialBlockRead(true);

  // Now we will look for a FAT partition!
  uint8_t part;
  for (part = 0; part < 5; part++) {     // we have up to 5 slots to look in
    if (vol.init(card, part))
      break;                             // we found one, lets bail
  }
  if (part == 5) {                       // if we ended up not finding one  :(
    putstring_nl("No valid FAT partition!");
    sdErrorCheck();      // Something went wrong, lets print out why
    while (1);                           // then 'halt' - do nothing!
  }

  // log found partition
  putstring("Using partition ");
  Serial.print(part, DEC);
  putstring(", type is FAT");
  Serial.println(vol.fatType(), DEC);    // FAT16 or FAT32?

  // Try to open the root directory
  if (!root.openRoot(vol)) {
    putstring_nl("Can't open root dir!"); // Something went wrong,
    while (1);                            // then 'halt' - do nothing!
  }

  // Past preflight checks
  putstring_nl("Ready!");

  TCCR2A = 0;
  TCCR2B = 1 << CS22 | 1 << CS21 | 1 << CS20;

  //Timer2 Overflow Interrupt Enable
  TIMSK2 |= 1 << TOIE2;

  // Start the I2C Bus as Master
  Wire.begin();
  
  //set party mode by holding down fire on power up
  if (digitalRead(buttons[0]) == LOW) {
    partyMode = true;
  }

  // set the speed at 60 rpm:
  stepper.setMaxSpeed(100);
  stepper.setAcceleration(300);
}

void loop() {
  //update timer
  t.update(); 
  
  if (partyMode) {
    // switch out of partyMode
    if ((justpressed[3] || justreleased[3]) && pressed[0]) {
      isVenting = pressed[3] == 1;
      justpressed[3] = 0;
      justreleased[3] = 0;
      partyMode = false;
      return;
    }
    
    if (justpressed[0]) {
      justpressed[0] = 0;
      playSong();
    }
    // autoplay next song
    if (!wave.isplaying) {
      playSong();
    }
  } else {    
    // is slo-blo toggled on (true for first loop)
    if (justpressed[1]) {
      justpressed[1] = 0;
      isSloBlo = true;
      sendIsSloBlo();
      stopOverheatTimer();
    } 
    if (justreleased[1]) {
      justreleased[1] = 0;
      isSloBlo = false;
      sendIsSloBlo();
    }

    // is vent toggled on
    if (justpressed[3]) {
      justpressed[3] = 0;
      isVenting = true;
      sendIsVenting();
      if (isStarted) {
        // TODO: recharge bar emit
//        clearSideBar();
        playfile("vent1.wav");
      }
    }

    if (justreleased[3]) {
      justreleased[3] = 0;
      isVenting = false;
      sendIsVenting();
      isIdle = true;
      if (pressed[0]) {
        partyMode = true;
        playSong();
        return;
      }
      playfile("pwrup.wav");
    }

    // intensify button
    if (justpressed[0]) {
      justpressed[0] = 0;
      if (!isVenting) {
        if (isSloBlo) {
          playfile("fire1_h.wav");
        } else {
          overheatTimer = t.after(11000, ventOverride);
          overheatSecondsTimer = t.every(1000, updateOverheatSeconds);
          // random pick from 2 blaster tracks
          randomFire = rand() % 2;
          if (randomFire == 1) {
            playfile("fire0_h.wav");
          } else {
            playfile("alt0_h1.wav");
          }
        }
        isIdle = false;
        sendIsFiring(1);
      } else {
        playfile("dryvent.wav");
      }
    }
    
    // firing
    if (pressed[0] && !isVenting) {
      if (isSloBlo && !isplaying("fire1_h.wav") && !isplaying("fire1_b.wav")) {
        playfile("fire1_b.wav");
      } else if (!isSloBlo && !isplaying("alt0_h1.wav") && !isplaying("fire0_h.wav")) {
        if (overheatSeconds > 8 && !isplaying("fire0_w.wav") && !isplaying("alt0_w.wav")) {
          Serial.println("begin overheat warning");
          if (randomFire == 1) {
            playfile("fire0_w.wav");
          } else {
            playfile("alt0_w.wav");
          }
        } else if (!isplaying("alt0_b.wav") 
          && !isplaying("fire0_w.wav") 
          && !isplaying("fire0_b.wav")
          && !isplaying("alt0_w.wav")) {
          if (randomFire == 1) {
            playfile("fire0_b.wav");
          } else {
            playfile("alt0_b.wav");
          }
          
        }
      }
          
        
    }
    //firing stopped
    if (justreleased[0]) {
      justreleased[0] = 0;
      if (!isVenting && !isCrossingStreams) {
        if (isSloBlo) {
          playfile("fire1_t.wav");
        } else {
          if (overheatSeconds > 7) {
            playfile("alt0_t3.wav");
          } else if (overheatSeconds > 4) {
            playfile("alt0_t2.wav");
          } else {
            playfile("alt0_t1.wav");
          }
          Serial.println("stop firing");
          stopOverheatTimer();
        }
        isIdle = true;
        sendIsFiring(0);
      }
    }

    // secondary button
    if (justpressed[2]) {
      justpressed[2] = 0;
      if (!isVenting) {
        playfile("alt1_h.wav");
        // TODO: secondary fire emit
//        digitalWrite(firingHat, HIGH);
//        startventHat();
//        runHighCyclotron();
        isIdle = false;
      }
    }
    // secondary firing
    if (pressed[2] && !isplaying("alt1_h.wav") && !isVenting) {
      playfile("alt1_h.wav");
    }
    // secondary firing stopped
    if (justreleased[2]) {
      justreleased[2] = 0;
      if (!isVenting) {
        isIdle = true;
      }
    }

    isCrossingStreams = false;
    // cross streams/instant overload
    if (pressed[0] && pressed[2]) {
      playcomplete("ovrld_h.wav");
      isCrossingStreams = true;
      playcomplete("vent2.wav");
      isIdle = true;
    }

    // handle inital startup sound
    if ((!isStarted && !isVenting) || isCrossingStreams) {
      playfile("pwrup.wav");
    }

    if (isIdle) {
      if (!isplaying("fire0_t.wav")
          && !isplaying("fire0_t.wav")
          && !isplaying("fire0_b.wav")
          && !isplaying("fire0_o.wav")
          && !isplaying("alt0_h.wav")
          && !isplaying("alt0_b.wav")
          && !isplaying("idle0_b.wav")
          && !isplaying("fire1_t.wav")
          && !isplaying("fire0_b.wav")
          && !isplaying("idle0_b.wav")
          && !isplaying("alt1_h.wav")
          && !isplaying("pwrup.wav")
          && !isplaying("alt0_t1.wav")
          && !isplaying("alt0_t2.wav")
          && !isplaying("alt0_t3.wav")
          && !isVenting) {
        playfile("idle0_b.wav");
      }
    }

    isStarted = true;
  }

  if (stepper.distanceToGo() == 0)  {
    stepper.moveTo(-stepper.currentPosition());
  }
  
  stepper.run();
}

#define EVENT_FIRING_START 1
#define EVENT_FIRING_END 2
#define EVENT_VENT_START 3
#define EVENT_VENT_END 4
#define EVENT_SLOBLO_ON 5
#define EVENT_SLOBLO_OFF 6
void sendIsFiring(byte isFiring) {
  Wire.beginTransmission(1);
  Wire.write(isFiring ? EVENT_FIRING_START : EVENT_FIRING_END);
  Wire.endTransmission();
}

void sendIsVenting() {
  Wire.beginTransmission(1);
  Wire.write(isVenting ? EVENT_VENT_START : EVENT_VENT_END);
  Wire.endTransmission();
}

void sendIsSloBlo() {
  Wire.beginTransmission(1);
  Wire.write(isSloBlo ? EVENT_SLOBLO_ON : EVENT_SLOBLO_OFF);
  Wire.endTransmission();
}

SIGNAL(TIMER2_OVF_vect) {
  check_switches();
}

void check_switches()
{
  static byte previousstate[NUMBUTTONS];
  static byte currentstate[NUMBUTTONS];
  byte index;

  for (index = 0; index < NUMBUTTONS; index++) {
    currentstate[index] = digitalRead(buttons[index]);   // read the button

    if (currentstate[index] == previousstate[index]) {
      if ((pressed[index] == LOW) && (currentstate[index] == LOW)) {
        // just pressed
        justpressed[index] = 1;
      }
      else if ((pressed[index] == HIGH) && (currentstate[index] == HIGH)) {
        // just released
        justreleased[index] = 1;
      }
      pressed[index] = !currentstate[index];  // remember, digital HIGH means NOT pressed
    }
    //Serial.println(pressed[index], DEC);
    previousstate[index] = currentstate[index];   // keep a running tally of the buttons
  }
}

void updateOverheatSeconds() {
  overheatSeconds += 1;
}

void ventOverride() {
  stopOverheatTimer();
  ejectRods();
  playcomplete("vent2.wav");
  
  retractRods();
  justreleased[0] = 0;
  playfile("pwrup.wav");
  isIdle = true;
}

void stopOverheatTimer() {
  t.stop(overheatTimer);
  t.stop(overheatSecondsTimer);
  overheatSeconds = 0;
}

void ejectRods() {
  Serial.println("eject");
  stepper.moveTo(1000);
//  stepper.step(stepsPerRevolution);
//  stepper.step(stepsPerRevolution);
//  stepper.step(stepsPerRevolution);  
}

void retractRods() {
  Serial.println("retract");
//  stepper.step(-stepsPerRevolution);
//  stepper.step(-stepsPerRevolution);
//  stepper.step(-stepsPerRevolution);
}

void playSong() {
  String filename;
  filename = "song";
  filename += curSong;
  filename += ".wav";
  playfile(string2char(filename));
  curSong++;
  if (curSong == 16) {
    curSong = 1;
  }
}

// Plays a full file from beginning to end with no pause.
void playcomplete(char *name) {
  // call our helper to find and play this name
  playfile(name);
  while (wave.isplaying) {
    // do nothing while its playing
  }
  // now its done playing
}

void playfile(char *name) {
  // see if the wave object is currently doing something
  if (wave.isplaying) {// already playing something, so stop it!
    wave.stop(); // stop it
  }
  // look in the root directory and open the file
  if (!f.open(root, name)) {
    putstring("Couldn't open file "); Serial.print(name); return;
  }
  // OK read the file and turn it into a wave object
  if (!wave.create(f)) {
    putstring_nl("Not a valid WAV"); return;
  }

  // ok time to play! start playback
  //set current file for is playing reference
  curfile = name;
  wave.play();
}

boolean isplaying(String name) {
  if (name == curfile && wave.isplaying) {
    return true;
  } else {
    return false;
  }
}

char* string2char(String command) {
  if (command.length() != 0) {
    char *p = const_cast<char*>(command.c_str());
    return p;
  }
}
